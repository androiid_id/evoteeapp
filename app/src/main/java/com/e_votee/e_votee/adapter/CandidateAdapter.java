package com.e_votee.e_votee.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.e_votee.e_votee.R;
import com.e_votee.e_votee.activity.DetailsCandidate;
import com.e_votee.e_votee.model.Candidate;
import com.e_votee.e_votee.rest.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by myself on 10/4/2016.
 */

public class CandidateAdapter extends RecyclerView.Adapter<CandidateAdapter.CandidateViewHolder> {

    private List<Candidate> candidates;
    private int rowLayout;
    private Context context;
    String url_img = "http://192.168.137.1:8000";

    public static class CandidateViewHolder extends RecyclerView.ViewHolder {

        CardView candidateLayout;
        TextView candidateName;
        ImageView candidatePct;


        public CandidateViewHolder(View v) {
            super(v);
            candidateLayout = (CardView) v.findViewById(R.id.candidate_layout);
            candidateName = (TextView) v.findViewById(R.id.name_candidate);
            candidatePct = (ImageView) v.findViewById(R.id.img_candidate);
        }
    }

    public CandidateAdapter(List<Candidate> candidates, int rowLayout, Context context) {
        this.candidates = candidates;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public CandidateAdapter.CandidateViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new CandidateViewHolder(view);
    }

    public void onBindViewHolder(CandidateViewHolder holder, final int position) {
        holder.candidateName.setText(candidates.get(position).getName());
        Picasso
                .with(context)
                .load(url_img+candidates.get(position).getPhoto())
                .fit() // will explain later
                .into((holder.candidatePct));


        holder.candidateLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(view.getContext());
                SharedPreferences.Editor editor = pref.edit();
                editor.putInt(Constants.cId, candidates.get(position).getId());
                editor.putString(Constants.cName, candidates.get(position).getName());
                editor.putString(Constants.cPhoto, candidates.get(position).getPhoto());
                editor.putString(Constants.cAbout, candidates.get(position).getAbout());
                editor.putString(Constants.Url_image, url_img);
                editor.apply();

                Intent i = new Intent(context, DetailsCandidate.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);

            }
        });

    }

    @Override
    public int getItemCount() {
        return candidates.size();
    }

}
