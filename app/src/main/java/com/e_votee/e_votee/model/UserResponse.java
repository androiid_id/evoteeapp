package com.e_votee.e_votee.model;

/**
 * Created by myself on 20/09/2016.
 * for handling Response which has three fields result, message and user.
 * The third field will be used only in Login Operation.
 */

public class UserResponse {

    private String token;
    private String user;
    private String email;
    private String password;
    private Integer score;
    private Integer candidate;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getCandidate() {
        return candidate;
    }

    public void setCandidate(Integer candidate) {
        this.candidate = candidate;
    }
}
