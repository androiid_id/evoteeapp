package com.e_votee.e_votee.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by myself on 10/3/2016.
 */

public class CandidateResponse {

    @SerializedName("user")
    private String user;
    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String description;
    @SerializedName("code")
    private String code;

    private String detail;

    @SerializedName("candidates")
    private List<Candidate> candidates = new ArrayList<Candidate>();

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<Candidate> getCandidates() {
        return candidates;
    }

    public void setCandidates(List<Candidate> candidates) {
        this.candidates = candidates;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
