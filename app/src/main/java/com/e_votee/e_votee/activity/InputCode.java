package com.e_votee.e_votee.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.e_votee.e_votee.R;
import com.e_votee.e_votee.model.Candidate;
import com.e_votee.e_votee.model.CandidateResponse;
import com.e_votee.e_votee.rest.ApiClient;
import com.e_votee.e_votee.rest.ApiInterface;
import com.e_votee.e_votee.rest.Constants;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.e_votee.e_votee.activity.Register.hideKeyboard;

/**
 * Created by myself on 22/09/2016.
 */
public class InputCode extends AppCompatActivity {

    EditText ET_input;
    Button input_bn;
    String code;
    String token;
    ProgressBar progressBar;
    LinearLayout ly;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_inputcode);

        ly = (LinearLayout) findViewById(R.id.layout_inputcode);
        ET_input = (EditText) findViewById(R.id.inputcode);
        input_bn = (Button) findViewById(R.id.btn_code);
        progressBar = (ProgressBar) findViewById(R.id.progress);

        //hide keyboard when user taps anywhere else on the screen in android
        ly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(InputCode.this);
            }
        });

        // Get token from Login response and save into preference
        SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, 0);
        token = "token " +pref.getString(Constants.Token,"");

        input_bn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                code = ET_input.getText().toString();

                if (!code.isEmpty() && !token.isEmpty()) {

                    requestcodeProsess(token, code);
                } else {

                    Toast.makeText(getApplicationContext(), "Please Input Code",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void requestcodeProsess (String token, String code) {

        ApiInterface apiCode = ApiClient.getClient().create(ApiInterface.class);
        Call<CandidateResponse> mService = apiCode.getCandidate(token, code);
        mService.enqueue(new Callback<CandidateResponse>() {
            @Override
            public void onResponse(Call<CandidateResponse> call, Response<CandidateResponse>
                    response) {
                if(response.code() == 200 ){

                    //get response body from retrofil callback
                    List<Candidate> candidates = response.body().getCandidates();
                    CandidateResponse rept = response.body();

                    //define share preference
                    SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, 0);
                    SharedPreferences.Editor editor = pref.edit();

                    //save response body to share preference
                    Gson gson = new Gson();
                    String jsonCandidate = gson.toJson(candidates);
                    editor.putString(Constants.CANDIDATE, jsonCandidate);
                    editor.putString(Constants.Tittle, rept.getTitle());
                    editor.apply();

                    Toast.makeText(InputCode.this, "Please Wait .......", Toast.LENGTH_LONG).show();
                    progressBar.setVisibility(View.VISIBLE);
                    new Handler().postDelayed(new Runnable() {

                        public void run() {
                            startActivity(new Intent(InputCode.this, ListCandidate.class));
                            finish();
                        }
                    }, 4000);

                } else {

                    Toast.makeText(InputCode.this, "Not Found in database, please contact admin " +
                            "maybe code is false ", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<CandidateResponse> call, Throwable t) {

                Toast.makeText(InputCode.this, "Please check your network connection and internet " +
                        "permission", Toast.LENGTH_LONG).show();
                progressBar.setVisibility(View.INVISIBLE);

            }
        });
    }
}
