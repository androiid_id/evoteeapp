package com.e_votee.e_votee.activity;

/**
 * Created by myself on 25/07/2016.
 */
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.e_votee.e_votee.R;
import com.e_votee.e_votee.model.UserResponse;
import com.e_votee.e_votee.rest.ApiClient;
import com.e_votee.e_votee.rest.ApiInterface;
import com.e_votee.e_votee.rest.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Register extends AppCompatActivity {

    Button reg_bn;
    EditText ET_USER_NAME,ET_USER_EMAIL,ET_USER_PASS;
    String username, mail, passwd;
    TextView textView;
    ProgressBar progressBar;
    LinearLayout ly;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_register);

        ly = (LinearLayout) findViewById(R.id.layout_register);
        reg_bn = (Button) findViewById(R.id.btn_register);
        ET_USER_NAME = (EditText) findViewById(R.id.username);
        ET_USER_EMAIL = (EditText) findViewById(R.id.usermail);
        ET_USER_PASS = (EditText) findViewById(R.id.userpass);
        textView = (TextView) findViewById(R.id.txt_signin);
        progressBar = (ProgressBar) findViewById(R.id.progress);

        //hide keyboard when user taps anywhere else on the screen in android
        ly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(Register.this);
            }
        });

        //Move to Login activity when clik
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Register.this, Login.class));
            }
        });

        reg_bn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username = ET_USER_NAME.getText().toString();
                mail = ET_USER_EMAIL.getText().toString();
                passwd = ET_USER_PASS.getText().toString();

                if (!username.isEmpty() && !mail.isEmpty() && !passwd.isEmpty()) {

                    progressBar.setVisibility(View.VISIBLE);
                    registerProsess(username, mail, passwd);

                } else {

                    Toast.makeText(getApplicationContext(), "Fields are empty !",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    private void registerProsess(String username, String mail,String passwd){
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<UserResponse> mService = apiService.regisUser(username,mail,passwd);
        mService.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {

                if (response.code() == 201) {

                    Toast.makeText(Register.this,"Register Success" , Toast.LENGTH_LONG).show();
                    startActivity(new Intent(Register.this, Login.class));

                } else {

                    Toast.makeText(Register.this,"A user with that username already exists." ,
                            Toast.LENGTH_LONG).show();

                }
                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {

                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(Register.this, "Please check your network connection and internet " +
                        "permission", Toast.LENGTH_LONG).show();

            }
        });
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
