package com.e_votee.e_votee.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.e_votee.e_votee.R;
import com.e_votee.e_votee.adapter.CandidateAdapter;
import com.e_votee.e_votee.model.Candidate;
import com.e_votee.e_votee.rest.Constants;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by myself on 10/4/2016.
 */

public class ListCandidate extends AppCompatActivity {

    List<Candidate> candidates;
    TextView txtTitlevote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycleview_candidate);

        //define share preference to retrive data
        final SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, 0);

        //get Tittle from class candidateresponse
        txtTitlevote =(TextView) findViewById(R.id.title_vote);
        txtTitlevote.setText(pref.getString(Constants.Tittle,""));

        //call RecycleView
        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.candidate_recycler_view);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //Retrive data share from share preference class candidate
        final String jsonCandidates = pref.getString(Constants.CANDIDATE, "");
        Gson gson = new Gson();
        final Candidate[] cItems = gson.fromJson(jsonCandidates,
                Candidate[].class);

        candidates = Arrays.asList(cItems);
        candidates = new ArrayList<Candidate>(candidates);

        //parse data into Recycleview
        recyclerView.setAdapter(new CandidateAdapter(candidates, R.layout.list_item_candidate,
                getApplicationContext()));
    }
}
