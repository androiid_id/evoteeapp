package com.e_votee.e_votee.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.e_votee.e_votee.R;
import com.e_votee.e_votee.model.UserResponse;
import com.e_votee.e_votee.rest.ApiClient;
import com.e_votee.e_votee.rest.ApiInterface;
import com.e_votee.e_votee.rest.Constants;
import com.squareup.picasso.Picasso;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by myself on 10/5/2016.
 */

public class DetailsCandidate extends AppCompatActivity {

    ImageView imvDetails;
    TextView txtnameDetails, txtAbout;
    Button btnVote;
    private Context context;
    String url_img;
    String token;
    String username;
    Integer id;
    Integer score = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_detailcandidat);

        imvDetails = (ImageView) findViewById(R.id.img_candidate_detail);
        txtnameDetails = (TextView) findViewById(R.id.tv_detail_name);
        txtAbout = (TextView) findViewById(R.id.tv_about_candidate);
        btnVote = (Button) findViewById(R.id.btn_vote);

        //retrive data from recycle view name, photo, detailinfo
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences
                (getApplicationContext());
        url_img = pref.getString(Constants.Url_image, "");
        txtnameDetails.setText(pref.getString(Constants.cName, ""));
        txtAbout.setText(pref.getString(Constants.cAbout, ""));
        Picasso
                .with(context)
                .load(url_img + pref.getString(Constants.cPhoto, ""))
                .fit()
                .into((imvDetails));

        btnVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //retrive data from login class get token and username
                SharedPreferences prefs = getSharedPreferences(Constants.PREFS_NAME, 0);
                token = "token " + prefs.getString(Constants.Token, "");
                username = prefs.getString(Constants.Username, "");

                //retrivedata from recycleview candidate get Id from candidate
                SharedPreferences prefsk = PreferenceManager.getDefaultSharedPreferences
                        (getApplicationContext());
                id = prefsk.getInt(Constants.cId, 0);

                // alaret diaolog confirm vote
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(DetailsCandidate.this);
                // Setting Dialog Title
                alertDialog.setTitle("Confirm Vote ");
                // Setting Dialog Message
                alertDialog.setMessage("Are you sure you want to give vote to this candidate ?");
                // Setting Positive "Yes" Button
                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to invoke YES event
                        if (token.isEmpty() && username.isEmpty() && id.equals(null)) {

                            Toast.makeText(getApplicationContext(), "Please Check your Configure again",
                                    Toast.LENGTH_LONG).show();
                        } else {
                            voteProsess(token, username, id, score);
                        }
                    }
                });
                // Setting Negative "NO" Button
                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                // Showing Alert Message
                alertDialog.show();
            }
        });
    }

    private void voteProsess(String token, String username, final Integer id, Integer score) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<UserResponse> mService = apiService.postVote(token, username, id, score);
        mService.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {

                if (response.code() == 201) {
                    Toast.makeText(DetailsCandidate.this, "Vote Sucess",
                            Toast.LENGTH_LONG).show();
                    new Handler().postDelayed(new Runnable() {

                        public void run() {
                            startActivity(new Intent(DetailsCandidate.this, NavigationHome.class));
                            finish();
                        }
                    }, 4000);

                } else if (response.code() == 400) {

                    Toast.makeText(DetailsCandidate.this, "You've Given Vote on this poll, " +
                            "Please enter other code ", Toast.LENGTH_LONG).show();
                    new Handler().postDelayed(new Runnable() {

                        public void run() {
                            startActivity(new Intent(DetailsCandidate.this, InputCode.class));
                            finish();
                        }
                    }, 4000);
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {

                Toast.makeText(DetailsCandidate.this, "Please check your network connection " +
                        "and internet permission", Toast.LENGTH_LONG).show();
            }
        });
    }
}
