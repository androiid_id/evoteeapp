package com.e_votee.e_votee.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.e_votee.e_votee.R;
import com.e_votee.e_votee.model.UserResponse;
import com.e_votee.e_votee.rest.ApiClient;
import com.e_votee.e_votee.rest.ApiInterface;
import com.e_votee.e_votee.rest.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.e_votee.e_votee.activity.Register.hideKeyboard;

/**
 * Created by myself on 25/07/2016.
 */
public class Login extends AppCompatActivity {

    TextView textView_reg;
    Button login_bn;
    EditText ET_user, ET_passwd;
    String username, passwd;
    ProgressBar progressBr;
    LinearLayout ly;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_login);

        ly = (LinearLayout) findViewById(R.id.layout_login);
        login_bn = (Button) findViewById(R.id.btn_login);
        ET_user = (EditText) findViewById(R.id.username);
        ET_passwd = (EditText) findViewById(R.id.userpass);
        textView_reg = (TextView) findViewById(R.id.txt_register);
        progressBr = (ProgressBar) findViewById(R.id.progress);

        //hide keyboard when user taps anywhere else on the screen in android
        ly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(Login.this);
            }
        });

        //Move to Register activity when clik
        textView_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Login.this, Register.class));
            }
        });

        login_bn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username = ET_user.getText().toString();
                passwd = ET_passwd.getText().toString();

                SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, 0);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString(Constants.Username, username);
                editor.apply();

                if (!username.isEmpty() && !passwd.isEmpty()) {

                    progressBr.setVisibility(View.VISIBLE);
                    loginProsess(username,passwd);

                } else {

                    Toast.makeText(getApplicationContext(), "Fields are empty !",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void loginProsess (String username, String passwd) {
        ApiInterface logService = ApiClient.getClient().create(ApiInterface.class);
        Call<UserResponse> mService = logService.loginUser(username, passwd);
        mService.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {

                UserResponse respt = response.body();
                progressBr.setVisibility(View.INVISIBLE);

                if(response.code() == 200) {

                    //Save Login Response as Token
                    SharedPreferences pref = getSharedPreferences(Constants.PREFS_NAME, 0);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString(Constants.Token, respt.getToken());
                    editor.apply();

                    Toast.makeText(Login.this, "Success Login as "+ pref.getString
                            (Constants.Username, "") , Toast.LENGTH_LONG).show();
                    startActivity(new Intent(Login.this, NavigationHome.class));

                } else if (response.code() == 400){

                    Toast.makeText(Login.this, "Wrong Username or Password" ,
                            Toast.LENGTH_LONG).show();
                } else {

                    Toast.makeText(Login.this, "Something wrong with server" ,
                            Toast.LENGTH_LONG).show();
                }
            }


            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {


                progressBr.setVisibility(View.INVISIBLE);
                Toast.makeText(Login.this, "Please check your network connection and internet " +
                        "permission", Toast.LENGTH_LONG).show();

            }
        });
    }
}
