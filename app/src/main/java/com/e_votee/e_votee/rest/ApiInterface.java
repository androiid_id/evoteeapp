package com.e_votee.e_votee.rest;

import com.e_votee.e_votee.model.CandidateResponse;
import com.e_votee.e_votee.model.UserResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;



/**
 * Created by myself on 20/09/2016.
 */
public interface ApiInterface {

    @FormUrlEncoded
    @POST("v1/register")
    Call<UserResponse> regisUser (@Field("username") String username,
                                  @Field("email") String email,
                                  @Field("password") String password
    );
    @FormUrlEncoded
    @POST("v1/login")
    Call<UserResponse> loginUser (@Field("username") String username,
                                   @Field("password") String password
    );

    @GET("v1/selection/get/{selection_code}")
    Call<CandidateResponse> getCandidate(@Header("Authorization") String token,
                                        @Path("selection_code") String code

    );

    @FormUrlEncoded
    @POST("v1/votes")
    Call<UserResponse> postVote(@Header("Authorization") String token,
                                     @Field("username") String username,
                                     @Field("candidate") Integer id,
                                     @Field("score") Integer score
    );


}
