package com.e_votee.e_votee.rest;

/**
 * Created by myself on 28/09/2016.
 */

public class Constants {

    public static final String Username = "nameKey";
    public static final String Email = "emailKey";
    public static final String Token = "tokenKey";
    public static final String PREFS_NAME = "MyPrefsFile";
    public static final String CANDIDATE = "Candidate_list";
    public static final String Tittle = "Tittle_vote";
    public static final String cId = "candidate_id";
    public static final String cName = "Candidate_name";
    public static final String cPhoto = "Candidate_photo";
    public static final String cAbout = "Candidate_about";
    public static final String Url_image = "http://192.168.43.134:8000";



}
